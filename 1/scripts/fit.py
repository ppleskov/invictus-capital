from utils import *

# removing nans and dups
def clean_data(df):
    print(len(df), "- original data size")
    df.dropna(inplace=True)
    print(len(df), "- after removing nans")
    df.drop_duplicates(inplace=True)
    print(len(df), "- after removing dups")
    df.reset_index(drop=True, inplace=True)
    print()
    return df


# fitting tfidf and saving the model
def tfidf(
    texts,
    model_path,
    analyzer="char_wb",
    ngram_range=(1, 3),
    max_features=500,
    max_df=0.7,
    norm="l1",
):
    vec = TfidfVectorizer(
        analyzer=analyzer,
        ngram_range=ngram_range,
        max_features=max_features,
        max_df=max_df,
        norm=norm,
    ).fit(texts)
    joblib.dump(vec, os.path.join(model_path, "tfidf.pkl"))
    return vec


# encoding target (str -> int)
def encode_labels(df, model_path):
    le = LabelEncoder()
    le.fit(df[TARGET])
    joblib.dump(le, os.path.join(model_path, "encoder.pkl"))
    y = le.transform(df[TARGET])
    return y


# standard scaling
def scale(X, model_path):
    scaler_filename = os.path.join(model_path, "scaler.pkl")
    scaler = StandardScaler()
    X_scaled = scaler.fit_transform(X)
    joblib.dump(scaler, scaler_filename)
    return X_scaled


# fitting logreg on 5 folds and saving the models
def train(X, y_true, C=1, solver="newton-cg", n_splits=5):
    models = []
    y_pred = np.zeros(X.shape[0])
    skf = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=RANDOM_SEED)
    for i, (train_index, valid_index) in enumerate(skf.split(X, y)):
        X_train = X[train_index, :]
        X_valid = X[valid_index, :]
        y_train = y[train_index]
        y_valid = y[valid_index]

        reg = LogisticRegression(
            C=C,
            solver=solver,
            class_weight="balanced",
            multi_class="multinomial",
            random_state=RANDOM_SEED,
            n_jobs=-1,
        ).fit(X_train, y_train)

        fold_pred = reg.predict(X_valid)
        y_pred[valid_index] = fold_pred
        evaluate(y_valid, fold_pred, prefix=f"Fold {i} ")
        models.append(reg)

    print()
    evaluate(y_true, y_pred, prefix="OOF ")
    joblib.dump(models, os.path.join(model_path, "reg.pkl"))
    return models


if __name__ == "__main__":
    file_path, model_path = read_args()

    # data reading and cleaning
    df = pd.read_csv(file_path)
    df = clean_data(df)

    # preprocessing and feature engineering
    texts = df[INPUT].apply(preprocessing).to_list()
    vec = tfidf(texts, model_path)

    # data preparation
    X = vec.transform(texts).toarray()
    X = scale(X, model_path)
    y = encode_labels(df, model_path)

    # model training
    models = train(X, y)
