from utils import *

# loading previously fitted scaler, label encoder, tfidf and regression models
def model_load(model_path):
    scaler = joblib.load(os.path.join(model_path, "scaler.pkl"))
    le = joblib.load(os.path.join(model_path, "encoder.pkl"))
    vec = joblib.load(os.path.join(model_path, "tfidf.pkl"))
    models = joblib.load(os.path.join(model_path, "reg.pkl"))
    return scaler, le, vec, models

# preprocessing, tfidf transformation, scaling and label encoding
def data_preparation(le, vec, file_path):
    dt = pd.read_csv(file_path)
    tmp = dt[INPUT].apply(preprocessing)
    X = vec.transform(tmp).toarray()
    X_scaled = scaler.transform(X)
    y_true = le.transform(dt[TARGET])
    return X_scaled, y_true

# making predictions for each fold, then majority voting
def predict(X, models):
    pred = np.zeros((X.shape[0], len(models)))
    for fold, model in enumerate(models):
        pred[:, fold] = model.predict(X)
    y_pred = np.array(stats.mode(pred, axis=1)[0])
    return y_pred


if __name__ == "__main__":
    file_path, model_path = read_args()

    # model and data preparation
    scaler, le, vec, models = model_load(model_path)
    X, y_true = data_preparation(le, vec, file_path)

    # predicton and evaluation
    y_pred = predict(X, models)
    evaluate(y_true, y_pred)
