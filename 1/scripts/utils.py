import os
import sys
import joblib
import scipy
import sklearn
import pandas as pd
import numpy as np

from scipy import stats
from sklearn.metrics import f1_score
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import StratifiedKFold
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import StandardScaler, LabelEncoder

assert "1.18.2" == np.__version__
assert "1.0.3" == pd.__version__
assert "1.4.1" == scipy.__version__
assert "0.14.1" == joblib.__version__
assert "0.22.2" == sklearn.__version__

INPUT = "text"
TARGET = "language"

RANDOM_SEED = 42

# parsing path to folder with model weights (to save/load) and text data (to fit/predict)
def read_args():
    args = sys.argv
    file_path = args[1]
    model_path = args[2]
    print("\n" + file_path)
    return file_path, model_path

# removing all non-alphabet chars except for apostrophe (special cases are considered) and space
def preprocessing(s):
    s = s.lower()
    s = s.replace("‘", "'")
    s = s.replace("’", "'")
    s = "".join([c for c in list(s) if c.isalpha() or c == "'" or c == " "])
    s = " ".join(s.split())
    return s

# F1 macro and micro metrics
def evaluate(y_true, y_pred, func=f1_score, prefix=""):
    print(prefix + "F1 macro:", func(y_true, y_pred, average="macro"))
    print(prefix + "F1 micro:", func(y_true, y_pred, average="micro"))
