### Folder structure

    .
    ├── input                    # input data: raw and preprocessed csv files
    ├── model                    # pickle model weights
    ├── nb                       # jupyter notebooks with research for the reference
    ├── scripts                  # main code
    ├── fit.sh                   # model training script
    ├── predict.sh               # model testing script
    ├── requirements.txt         # run *pip install -r* before executing main scripts
    └── README.md
