# Invictus Capital

```console
pip install -r requirements.txt
sh fit.sh
sh predict.sh
```

[Task 1 report](https://docs.google.com/document/d/1VXT4kAX1aDmghIH8NwF3VkENOrO6_-D21QCtusxgduc)

[Task 2 report](https://docs.google.com/document/d/1FWNSma9_PGrCXmvivqKiaWVElpJ_QsrWfmRZRyLN-dQ)

[Bonus question](https://docs.google.com/document/d/1Ra3OzARNaL2w-5Hs__TGr5MFmg8LxfJMkNWd414GXZ4)

